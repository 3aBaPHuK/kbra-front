import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const routes = [
  {path: '/', component: () => import('../views/Main/MainComponent.vue')},
  {
    path: '/products/',
    component: () => import('../views/Products/ProductsComponent.vue')
  },
  {
    path: '/products/:category',
    component: () => import('../views/Products/ProductsComponent.vue')
  },
  {
    path: '/products/:category/:product',
    component: () => import('../views/Products/Card/ProductCard.vue')
  },
  {
    path: '/order/confirmed',
    component: () => import('../views/CheckoutOrder/OrderConfirmed.vue')
  },
  {
    path: '/order/confirmed/:orderId',
    component: () => import('../views/CheckoutOrder/OrderConfirmed.vue')
  },
  {path: '/contacts/', component: () => import('../views/Contacts/ContactsComponent.vue')},
  {path: '/checkout/', component: () => import('../views/CheckoutOrder/CheckoutOrder.vue')},
  {path: '/offer/', component: () => import('../views/Offer/OfferComponent.vue')},
  {
    path: '/articles/:slug',
    component: () => import('../views/Articles/ArticlesComponent.vue')
  },
  {
    name: 'articles',
    path: '/articles/:slug/:articleSlug',
    component: () => import('../views/Articles/ArticlesComponent.vue')
  },
  {
    path: '/gallery/',
    component: () => import('../views/Gallery/GalleryComponent.vue')
  },
  {
    path: '/gallery/:id',
    component: () => import('../views/Gallery/GalleryComponent.vue')
  },
  {
    path: '/cart',
    component: () => import('../views/Cart/Cart.vue')
  },
  {
    path: '/compare',
    component: () => import('../views/Compare/CompareComponent.vue')
  },
  {
    path: '*',
    component: () => import('../views/NotFound.vue'),
    notFoundPage: true
  }
];

export function createRouter () {
  return new Router({
    routes,
    mode: 'history',
    fallback: false,
    scrollBehavior: (to) => {
      if (to.hash) {
        return {selector: to.hash}
      } else {
        return { x: 0, y: 0 }
      }
    }
  })
}
