import { createApp } from './app'
import configData from '../config.json';

const isDev = process.env.NODE_ENV !== 'production'

let version = null;

if (isDev) {
  const { exec } = require('child_process');
  exec('git rev-parse --abbrev-ref HEAD', (err, stdout, stderr) => {
    if (typeof stdout === 'string') {
      version = stdout.trim();
    }
  });
}

export default context => {
  return new Promise(async (resolve, reject) => {
    const s = isDev && Date.now()
    const { app, router, store } = createApp();

    store.commit('setConfigData', configData)
    store.commit('setVersion', version)
    await store.dispatch('updateShop')
    await store.dispatch('fetchPointsOfSales')

    const { url } = context
    const { fullPath } = router.resolve(url).route

    if (fullPath !== url) {
      return reject({ url: fullPath })
    }

    await router.push(url)

    router.onReady(() => {
      const matchedComponents = router.getMatchedComponents()
      if (!matchedComponents.length) {
        return reject({ code: 404 })
      }

      Promise.all(matchedComponents.map(({ asyncData }) => asyncData && asyncData({
        store,
        route: router.currentRoute
      }))).then(() => {
        isDev && console.log(`data pre-fetch: ${Date.now() - s}ms`)
        context.state = store.state
        context.meta = app.$meta()
        context.counters = store.getters.getShopData.counters.map(counter => `${counter.code}\n`);
        resolve(app)
      }).catch(reject)
    }, reject)
  })
}
