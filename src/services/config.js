
export class Config {
    constructor(data) {
        this.data = data;
    }

    getParameter(key) {
        if (typeof this.data[key] !== 'undefined') {
            return this.data[key]
        }

        return null;
    }

}
