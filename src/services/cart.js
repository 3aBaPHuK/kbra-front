export class Cart {
    createCartItemFromProduct(product, offer, amount, configurationValues) {
        return {
            offerId: offer.id,
            productId: product.id,
            amount: amount,
            name: offer.name || product.name,
            price: offer.discountPrice || offer.price,
            originalPrice: offer.discountPrice || offer.price,
            image: offer.cover || product.cover,
            productSlug: product.canonical,
            categorySlug: product.categorySlug,
            configurationValues
        }
    }

    createCartItemFromPresent(present, productId) {
        if (!present || !present.presentProduct || !present.presentProduct.offers.length) {
            return null;
        }

        return {
            offerId: present.presentProduct.offers[0].id,
            productId: present.presentProduct.id,
            amount: 1,
            name: present.presentProduct.name,
            price: 0,
            image: present.presentProduct.cover,
            productSlug: present.presentProduct.canonical,
            categorySlug: present.presentProduct.categorySlug,
            isPresent: true,
            presentFor: productId
        }
    }
}
