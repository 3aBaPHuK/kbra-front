import axios from 'axios'
import btoa from 'btoa';

export class Api {
    constructor() {
    }

    setConfig(config) {
        this.config = config;
    }

    getProductsBySlug(slug) {
        return this.request('GET', this.config.apiBaseUrl + `category/${slug}/products`)
    }

    createOrder(data) {
        return this.request('POST', this.config.apiBaseUrl + `order/create`, data)
    }

    getOrder(hash) {
        return this.request('GET', this.config.apiBaseUrl + `order/${hash}`)
    }

    getProductsById(id) {
        return this.request('GET', this.config.apiBaseUrl + `products/${id}`)
    }

    async getProductsListByIds(ids)
    {
        return this.request('POST', this.config.apiBaseUrl + 'products/list/byIds', ids)
    }

    getArticlesCategoryBySlug(slug) {
        return this.request('GET', this.config.apiBaseUrl + `category/${slug}`);
    }

    getAllPointsOfSales() {
        return this.request('GET', this.config.apiBaseUrl + `points-of-sale`);
    }

    getCategoryBySlug(slug) {
        return this.request('GET', this.config.apiBaseUrl + `categories/${slug}`);
    }

    async getSliderByAlias(alias) {
        return this.request('GET', this.config.apiBaseUrl + `slides/${alias}`);
    }

    getProductBySlugAndCategory(productSlug, categorySlug) {
        return this.request('GET', this.config.apiBaseUrl + `${categorySlug}/product/${productSlug}`)
    }

    async getPresentByProductId(id) {
        return this.request('GET', this.config.apiBaseUrl + `product/${id}/present`)
    }

    async getProductConfigurationBySlug(categorySlug, productSlug, selectedValues) {
        return this.request('GET', this.config.apiBaseUrl + `product/${categorySlug}/${productSlug}/configuration`, selectedValues)
    }

    getCategories() {
        return this.request('GET', this.config.apiBaseUrl + 'categories');
    }

    getGalleries() {
        return this.request('GET', this.config.apiBaseUrl + 'galleries');
    }

    getGalleryItemsById(id) {
        return this.request('GET', this.config.apiBaseUrl + `gallery/items/${id}`);
    }

    async getShopData() {
        return this.request('GET', this.config.apiBaseUrl + `shop/active`);
    }

    async getActualPromotion() {
        return this.request('GET', this.config.apiBaseUrl + `promotion/active`);
    }

    async getAhunterSuggestions(query) {
        return axios.request({method: 'GET', url: this.config.ahunterBaseUrl + `site/suggest/address?output=json;query=${query}`})
    }

    async getOfferPricesByCouponCode(couponCode, offerIds) {
        return this.request(
          'POST',
          `${this.config.apiBaseUrl}coupon/calculate-discount`,
          {
              couponCode: couponCode,
              offerIds: offerIds
          }
        )
    }

    request(method, url, body = null) {
        return axios.request({
            method: method,
            url: url,
            headers: {
                'Content-Type': 'application/json;charset=utf-8',
                'Authorization': 'Basic ' + btoa(this.config.apiLogin + ':' + this.config.apiPass)
            },
            data: body
        })
    }
}
