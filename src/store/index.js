import Vue from 'vue'
import Vuex from 'vuex'
import actions from './actions'
import mutations from './mutations'
import getters from './getters'
import shop from './modules/shop';
import menu from './modules/menu';
import articles from './modules/articles';
import articlesCategory from './modules/articlesCategory';
import categories from './modules/categories';
import galleries from './modules/galleries';
import preloader from './modules/preloader';
import products from './modules/products';
import slides from './modules/slides';
import config from './modules/config';
import version from "./modules/version";
import checkout from "./modules/checkout";
import cart from "./modules/cart";
import pointsOfSale from "./modules/pointsOfSale";
import compare from "./modules/compare";
import promotions from "./modules/promotions";

Vue.use(Vuex)

export function createStore () {
  return new Vuex.Store({
    modules: [config, shop, menu, articles, articlesCategory, categories, galleries, preloader, products, slides, version, checkout, cart, pointsOfSale, compare, promotions],
    state: {
      cryptoRates: null
    },
    actions,
    mutations,
    getters
  })
}
