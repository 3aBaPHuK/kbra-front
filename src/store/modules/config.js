export default {
  state: {
    isNotFound: false,
    config: {}
  },
  getters: {
    getConfig: state => state.config,
    getParameter: state => key => {
      if (typeof state.config[key] === 'undefined') {
        return null;
      }

      return state.config[key];
    },
  },
  mutations: {
    setNotFound(state) {
      state.isNotFound = true;
    },
    resetNotFound(state) {
      state.isNotFound = false;
    },
    setConfigData: (state, data) => {
      state.config = data;
    }
  }
}
