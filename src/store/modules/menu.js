export default {
    state: {
        items: [
            {
                title: 'Главная',
                link: '/',
            },
            {
                title: 'Производство',
                link: '/articles/production',
            },
            {
                title: 'Товары',
                link: '/products'
            },
            {
                title: 'Гарантии и сервис',
                link: '/articles/warranty/garantii-i-servis',
            },
            {
                title: 'Доставка и оплата',
                link: '/articles/delivery_payment/oplata',
            },
            {
                title: 'Контакты',
                link: '/contacts',
            },
        ]
    },
    getters: {
        getMenuItems: state => state.items
    }
}
