import {Api} from "../../services/api";

export default {
    state: {
        pointsOfSales: [],
        daysOfWeekTranslation: {
            mon: 'Пн',
            tue: 'Вт',
            wed: 'Ср',
            thu: 'Чт',
            fri: 'Пт',
            sat: 'Сб',
            sun: 'Вс',
        },
    },
    getters: {
        getPointsOfSales: state => state.pointsOfSales,
        getDaysOfWeekTranslation: state => state.daysOfWeekTranslation,
    },
    actions: {
        fetchPointsOfSales({commit}) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getAllPointsOfSales().then(data => {
                commit('updatePointsOfSales', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updatePointsOfSales: (state, data) => {
            state.pointsOfSales = data;
        }
    }
}
