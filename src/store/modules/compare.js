export default {
    state: {
        items: []
    },
    actions: {
        removeProductFromCompare({state, commit}, product) {
            commit('setItems', state.items.filter(item => item.id !== product.id))
        },
        addProductToCompare({state, commit}, product) {
            if (state.items.find(item => item.id === product.id)) {
                return;
            }

            commit('setItems', [...state.items, product]);
        }
    },
    mutations: {
        initCompare(state) {
            const compareItems = localStorage.getItem('compare');

            if (!compareItems) {
                return;
            }

            state.items = JSON.parse(compareItems)
        },
        setItems(state, items) {
            state.items = items;
            localStorage.setItem('compare', JSON.stringify(state.items));
        }
    },
    getters: {
        compareItems: state => state.items,
        getItemByProductAndLabel: state => (type, productId, label) => {
            const product = state.items.find(item => item.id === productId);

            if (!product || typeof product[type] === 'undefined') {
                return null;
            }

            const property = product[type].find(item => item.label === label);

            return property ? property.value : null;
        },
        getUniqueLabels: state => (prop) => {
            const uniqueLabels = [];

            state.items.forEach(product => {
                if (typeof product[prop] === 'undefined') {
                    return;
                }

                product[prop].forEach(item => {
                    if (!uniqueLabels.find(property => property === item.label)) {
                        uniqueLabels.push(item.label);
                    }
                })
            })

            return uniqueLabels;
        },
        equipments: state => {},
        isProductInCompare: state => id => !!state.items.find(item => item.id === id),
        compareIsAvailable: state => state.items.length < 4
    }
}
