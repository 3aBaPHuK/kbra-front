import { Api } from '../../services/api'
import { Cart } from '../../services/cart'
import Vue from 'vue'

export default {
    state: {
        total: 0,
        oneClickProductConfiguration: null,
        orderItems: [],
        personalDataPublicOffer: 'Я, заполняя форму на сайте lodki.shop, даю свое согласие администратору сайта lodki.shop, в соответствии с Федеральным законом от 27.07.2006 152-ФЗ «О персональных данных», на обработку, хранение и передачу третьим лицам через Интернет моих персональных данных.<br><br> Я подтверждаю, что, даю такое согласие, действуя по своей воле и в своем интересе. Целью предоставления мною персональных данных является установление связи с администратором сайта lodki.shop, включая направление уведомлений, запросов, касающихся использования сайта, оказания услуг. Настоящим, я признаю и подтверждаю, что я самостоятельно и полностью несу ответственность за предоставленные мною персональные данные, включая их полноту, достоверность, недвусмысленность и относимость непосредственно ко мне.<br><br> Я подтверждаю, что ознакомлен с правами и обязанностями, в соответствии с Федеральным законом «О персональных данных», в т.ч. порядком отзыва согласия на сбор и обработку персональных данных.',
    },
    actions: {
        createOneClickOrderItem({state, commit}, oneClickItem) {
            commit('clearOrderItems');

            const api = new Api();
            const cartService = new Cart();

            api.setConfig(this.getters.getConfig);

            return api.getProductsById(oneClickItem.productId).then(async data => {
                const oneClickProduct = data.data;

                if (!oneClickProduct) {
                    return;
                }


                const oneClickOffer = oneClickProduct.offers.find(offer => offer.id === oneClickItem.offerId);

                if (!oneClickOffer) {
                    return;
                }

                const price = oneClickOffer.discountPrice || oneClickOffer.price;

                state.total = price * oneClickItem.amount;
                oneClickItem.name = oneClickProduct.name;

                oneClickItem.price = oneClickItem.isPresent ? 0 : price;

                commit('addOrderItem', oneClickItem);

                const presentResponse = await api.getPresentByProductId(oneClickProduct.id);

                if (presentResponse) {
                    const presentCartItem = cartService.createCartItemFromPresent(presentResponse.data);

                    commit('addOrderItem', presentCartItem);
                }
            }).catch(e => {
                console.log(e)
            })
        },
        createOrderItems({state, commit}, cartItems) {
            commit('clearOrderItems');

            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductsListByIds(cartItems.map(cartItem => cartItem.productId)).then(async data => {
                let discountedByCouponPricesResponse = null;

                if (this.getters.getCart.couponCode) {
                    discountedByCouponPricesResponse = await api.getOfferPricesByCouponCode(this.getters.getCart.couponCode, cartItems.map(cartItem => cartItem.offerId));
                }

                cartItems.forEach(cartItem => {
                    const orderItem = Object.assign({name: '', price: 0}, cartItem);
                    const cartItemProduct = data.data.find(product => product.id === cartItem.productId);

                    if (!cartItemProduct) {
                        return;
                    }

                    const cartItemOffer = cartItemProduct.offers.find(offer => offer.id === cartItem.offerId);

                    if (!cartItemOffer) {
                        return;
                    }
                    let price = cartItemOffer.discountPrice || cartItemOffer.price;

                    if (discountedByCouponPricesResponse) {
                        const couponPrice = discountedByCouponPricesResponse.data.find(cPrice => cPrice.offerId === cartItemOffer.id);

                        if (couponPrice) {
                            price = couponPrice.price;
                        }
                    }

                    if (!cartItem.isPresent) {

                        state.total += price * cartItem.amount;
                    }

                    orderItem.name = cartItemOffer.name;
                    orderItem.price = orderItem.isPresent ? 0 : price;
                    orderItem.configurationValues = cartItem.configurationValues;

                    commit('addOrderItem', orderItem);
                });
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        addOneClickProductConfiguration(state, configuration) {
            state.oneClickProductConfiguration = configuration;

            localStorage.setItem('oneClickProductConfiguration', JSON.stringify(state.oneClickProductConfiguration));
        },
        clearOneClickProductConfiguration(state) {
            state.oneClickProductConfiguration = null;
            localStorage.removeItem('oneClickProductConfiguration');
        },
        clearOrderItems(state) {
            state.orderItems = [];
            state.total = 0;
        },
        addOrderItem(state, orderItem) {
            state.orderItems.push(orderItem)
        },
        updateProducts: (state, data) => {
            Vue.set(state, 'products',  data);
        },
    },
    getters: {
        personalDataPublicOfferHtml: state => state.personalDataPublicOffer,
        orderItems: state => state.orderItems,
        orderTotal: state => state.total,
        oneClickProductConfiguration: state => {
            if (state.oneClickProductConfiguration) {
                return state.oneClickProductConfiguration;
            }

            const stored = JSON.parse(localStorage.getItem('oneClickProductConfiguration'));

            if (stored) {
                return stored;
            }

            return null;
        }
    }
}
