import {Api} from "../../services/api";
import Vue from "vue";

export default {
    state: {
        products: [],
        product: null,
        currentCategory: null
    },
    getters: {
        getProducts: state => state.products,
        getProduct: state => state.product
    },
    actions: {
        fetchProducts({store, commit}, activeSlug) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductsBySlug(activeSlug).then(data => {
                commit('updateProducts', data.data)
            }).catch(e => {
                console.log(e)
            })
        },
        fetchProductBySlugAndCategory({store, commit}, {category, product}) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductBySlugAndCategory(product, category).then(data => {
                commit('updateProduct', data.data)
            }).catch(e => {
                console.log(e)

                throw {code: 404}
            })
        },
        fetchProductById({store, commit}, id) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductsById(id).then(data => {
                commit('updateProduct', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updateProducts: (state, data) => {
            Vue.set(state, 'products',  data);
        },
        updateProduct: (state, data) => {
            state.product = data;
        }
    }
}
