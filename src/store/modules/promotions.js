import {Api} from "../../services/api";

export default {
    state: {
        promotion: null
    },
    getters: {
        activePromotion: state => state.promotion,
    },
    actions: {
        fetchActivePromotion({commit}) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getActualPromotion().then(data => {
                commit('updatePromotion', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updatePromotion: (state, data) => {
            state.promotion = data;
        }
    }
}
