import {Api} from "../../services/api";

export default {
    state: {
        articles: [],
        contactsArticles: [],
    },
    getters: {
        getArticles: state => state.articles,
        getContactsArticles: state => state.contactsArticles,
    },
    actions: {
        fetchContactsArticles({commit}) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getArticlesCategoryBySlug('CONTACTS').then(data => {
                commit('updateContactsArticles', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updateArticles: (state, data) => {
            state.articles = data;
        },
        updateContactsArticles: (state, data) => {
            state.contactsArticles = data;
        }
    }
}
