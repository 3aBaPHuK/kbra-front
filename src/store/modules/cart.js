import { Api } from '../../services/api';

export default {
    state: {
        cart: {
            items: [],
            total: 0,
            couponCode: ''
        },
        couponErrorMessage: null
    },
    actions: {
        resetCoupon({state, commit}) {
            state.cart.couponCode = '';
            state.cart.couponErrorMessage = null;
            state.cart.items.forEach(cartItem => {
                if (cartItem.isPresent) {
                    return;
                }

                cartItem.price = cartItem.originalPrice
            })

            commit('setCart', {cart: state.cart});
        },
        async applyCouponCode({state, commit}, couponCode) {
            state.cart.couponCode = couponCode;
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            commit('setCart', {cart: state.cart, api});
        },
        decreaseAmount({state, commit}, index) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            if (state.cart.items[index].isPresent) {
                return;
            }

            if (state.cart.items[index].amount <= 1) {
                return;
            }

            state.cart.items[index].amount--;
            commit('setCart', {cart: state.cart, api});
        },
        increaseAmount({state, commit}, index) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            if (state.cart.items[index].isPresent) {
                return;
            }

            state.cart.items[index].amount++;
            commit('setCart', {cart: state.cart, api});
        },
        clearCart({commit}) {
            commit('setCart', {
                cart: {
                    items: [],
                    total: 0,
                    couponCode: ''
                }
            });
        },
        removeFromCart({state, commit}, cartItem) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            state.cart.items.splice(state.cart.items.findIndex(item => item.offerId === cartItem.offerId), 1);

            const presentIndex = state.cart.items.findIndex(item => item.isPresent && item.presentFor === cartItem.productId)

            if (presentIndex !== -1) {
                state.cart.items.splice(presentIndex, 1);
            }

            commit('setCart', {cart: state.cart, api});
        },
        addToCart({state, commit}, cartItem) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            const existing = state.cart.items.find(item => item.offerId === cartItem.offerId);

            if (existing) {
                if (cartItem.isPresent) {
                    return;
                }

                existing.amount += cartItem.amount;
            } else {
                state.cart.items.push(cartItem)
            }

            commit('setCart', {cart: state.cart, api});
        }
    },
    getters: {
        getCart: state => state.cart,
        getCouponErrorMessage: state => state.couponErrorMessage
    },
    mutations: {
        initCart: state => {
            const storedCart = localStorage.getItem('cart');

            if (storedCart) {
                state.cart = JSON.parse(storedCart);
            }
        },
        setCart: async (state, {cart, api}) => {
            state.cart.couponErrorMessage = null;

            if (api) {
                try {
                    const offerIds = state.cart.items.map(item => item.offerId);
                    const discountedPricesResponse = await api.getOfferPricesByCouponCode(state.cart.couponCode, offerIds);

                    if (!discountedPricesResponse.data || !discountedPricesResponse.data.length) {
                        return;
                    }

                    discountedPricesResponse.data.forEach(discountedPrice => {
                        const cartItem = state.cart.items.find(item => item.offerId === discountedPrice.offerId);

                        cartItem.price = discountedPrice.price;
                    });
                } catch (e) {
                    if (e.response.status) {
                        state.couponErrorMessage = `Неизвестный промокод: "${state.cart.couponCode}"`
                        state.cart.couponCode = ''

                        setTimeout(() => {
                            state.couponErrorMessage = '';
                        }, 2000)
                    }
                }
            }

            state.cart = cart;
            state.cart.total = 0;
            state.cart.items.forEach(cartItem => {
                state.cart.total += cartItem.price * cartItem.amount
            });


            localStorage.setItem('cart', JSON.stringify(state.cart));
        }
    }
}
