import { Api } from '../../services/api'

export default {
    state: {
        slider: null
    },
    getters: {
        getSlider: state => state.slider,
    },
    actions: {
        updateSlider({ commit }) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)
            return api.getSliderByAlias('MAIN').then(data => {
                commit('updateSlider', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updateSlider: (state, data) => {
            state.slider = data;
        }
    }
}
