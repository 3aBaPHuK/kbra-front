import {Api} from "../../services/api";

export default {
    state: {
        categories: [],
        activeId: null,
        activeSlug: null
    },
    actions: {
        fetchCategories({store, commit}, activeSlug) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            return api.getCategories().then(data => {
                commit('updateCategories', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    getters: {
        getActiveId: state => state.activeId,
        getActiveSlug: state => state.activeSlug,
        getCategoryById: state => id => state.categories.find(category => category.id === id),
        getRootCategories: state => state.categories.filter(category => category.parentId === null),
        categories: state => state.categories,
        activeCategory: state => state.categories.find(category => category.canonical === state.activeSlug)
    },
    mutations: {
        updateCategories: (state, data) => {
            state.categories = data;
        },
        setActiveId: (state, id) => {
            state.activeId = id;
        },
        setActiveSlug: (state, slug) => {
            state.activeSlug = slug;
        }
    }
}
