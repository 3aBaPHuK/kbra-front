export default {
    state: {
        version: null
    },
    getters: {
        getVersion: state => state.version,
    },
    mutations: {
        setVersion: (state, version) => {
            state.version = version;
        }
    }
}
