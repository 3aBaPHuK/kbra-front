import {Api} from "../../services/api";
import Vue from 'vue';

export default {
    state: {
        galleries: [],
        activeGalleryId: null,
        items: []
    },
    getters: {
        getGalleryById: state => id => state.galleries.find(gallery => gallery.id === id),
        getGalleries: state => state.galleries,
        getActiveGalleryId: state => state.activeGalleryId,
        getActiveGalleryType: state => {
            const activeGallery = state.galleries.find(gallery => gallery.id === state.activeGalleryId);

            if (!activeGallery) {
                return null;
            }

            return activeGallery.type;
        },
        getActiveGalleryItems: state => state.items
    },
    actions: {
        fetchGalleries({store, commit}, id) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            return api.getGalleries().then(data => {
                commit('updateGalleries', data.data);

                if (data.data.length) {
                    commit('setActiveGalleryId', id ? id : data.data[0].id)
                }
            });
        },
        fetchGalleryItems({store, commit}, id) {
            const api = new Api();
            api.setConfig(this.getters.getConfig);

            return api.getGalleryItemsById(id).then(data => {
                commit('setActiveGalleryItems', data.data);
            });
        }
    },
    mutations: {
        updateGalleries: (state, data) => {
            state.galleries = data;
        },
        setActiveGalleryId: (state, id) => state.activeGalleryId = id,
        setActiveGalleryItems: (state, items) => state.items = items
    }
}
