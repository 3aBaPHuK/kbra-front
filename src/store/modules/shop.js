import { Api } from '../../services/api'

export default {
    state: {
        shop: null
    },
    actions: {
        updateShop({ commit, state }) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getShopData().then(data => {
                commit('setShopData', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    getters: {
        getShopData: state => state.shop,
    },
    mutations: {
        setShopData: (state, data) => {
            state.shop = data;
        }
    }
}
