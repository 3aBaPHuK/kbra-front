import {Api} from "../../services/api";

export default {
    state: {
        products: [],
        product: null,
        currentCategory: null
    },
    getters: {
        getProducts: state => state.products,
        getProduct: state => state.product
    },
    actions: {
        fetchProducts({store, commit}, activeSlug) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductsBySlug(activeSlug).then(data => {
                commit('updateProducts', data.data)
            }).catch(e => {
                console.log(e)
            })
        },
        fetchProductBySlug({store, commit}, slug) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getProductBySlug(slug).then(data => {
                commit('updateProduct', data.data)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updateProducts: (state, data) => {
            state.products = data;
        },

        updateProduct: (state, data) => {
            state.product = data;
        }
    }
}
