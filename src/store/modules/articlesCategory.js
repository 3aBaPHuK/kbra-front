import {Api} from "../../services/api";
import Vue from "vue";

export default {
    state: {
        category: {},
        activeArticleSlug: null
    },
    getters: {
        getCategory: state => state.category,
        getCategoryArticles: state => state.category.articles,
        getActiveArticleSlug: state => state.activeArticleSlug,
    },
    actions: {
        fetchCategoriesAndArticles({store, commit}, {categorySlug, articleSlug}) {
            const api = new Api();
            api.setConfig(this.getters.getConfig)

            return api.getArticlesCategoryBySlug(categorySlug).then(data => {
                data.data.articles = data.data.articles.sort((a, b) => a.id - b.id);

                commit('updateArticleCategory', data.data)
                commit('setActiveArticleSlug', articleSlug ? articleSlug : data.data.articles[0].slug)
            }).catch(e => {
                console.log(e)
            })
        }
    },
    mutations: {
        updateArticleCategory: (state, data) => {
            Vue.set(state, 'category', data);
        },
        setActiveArticleSlug: (state, slug) => state.activeArticleSlug = slug
    }
}
