import Vue from 'vue';
import Meta from 'vue-meta';
import App from './App.vue';
import { createStore } from './store';
import { createRouter } from './router';
import { sync } from 'vuex-router-sync';
import {Api} from "./services/api";
import VueMask from 'v-mask';
import VTooltip from 'v-tooltip';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import PortalVue from 'portal-vue'
import Vue2TouchEvents from 'vue2-touch-events'
import Autocomplete from '@trevoreyre/autocomplete-vue'
import '@trevoreyre/autocomplete-vue/dist/style.css'

import 'bootstrap/dist/css/bootstrap.css';

import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'public/assets/css/main.css';
import 'public/assets/css/articles.css';
import 'public/assets/css/tooltip.css';
import 'public/assets/css/map.css';
Vue.use(VTooltip);

Vue.use(VueMask);
Vue.use(Meta);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(PortalVue);
Vue.use(Vue2TouchEvents)
Vue.use(Autocomplete)

import 'core-js/stable';
import {Cart} from "./services/cart";

export function createApp () {
  const store = createStore();
  const router = createRouter();

  sync(store, router);

  Vue.prototype.$services = {
    'api': new Api(store.getters.getConfig),
    'cart': new Cart()
  };

  const app = new Vue({
    metaInfo: {
      title: 'РУССКИЕ АМФИБИИ - ОФИЦИАЛЬНЫЙ МАГАЗИН ПРОИЗВОДИТЕЛЯ ЛОДОК',
      meta: [
        { vmid: 'description', name: 'description', content: 'РУССКИЕ АМФИБИИ - ОФИЦИАЛЬНЫЙ МАГАЗИН ПРОИЗВОДИТЕЛЯ ЛОДОК' }
      ],
      titleTemplate: '%s | РУССКИЕ АМФИБИИ',
      htmlAttrs: {
        lang: 'en'
      }
    },
    router,
    store,
    render: h => h(App)
  });

  return { app, router, store }
}
