<template>
    <div>
        <div>
            <ul class="list-group mb-5" v-for="(item, key) in getCart.items">
                <li class="list-group-item d-flex align-items-center">
                    <div class="title"><router-link :to="`/products/${item.categorySlug}/${item.productSlug}`">{{ item.name }}</router-link></div>
                    <div class="d-flex justify-content-end flex-grow-1">
                        <a href="javascript:void(0)" @click.stop
                           @click="$store.dispatch('removeFromCart', item)">
                            <b-icon-trash scale="1.5"></b-icon-trash>
                        </a>
                    </div>
                </li>
                <li class="list-group-item d-flex align-items-center">
                    <div class="label flex-grow-1">Количество:</div>
                    <div class="value">
                        <div class="form-group d-flex align-items-center mb-0 justify-content-end">
                            <a href="javascript:void(0)" @click="$store.dispatch('decreaseAmount', key)"
                               class="amount-control pr-3">-</a>
                            <input type="text" disabled="disabled" class="form-control w-25 text-center"
                                   v-model="item.amount">
                            <a href="javascript:void(0)" @click="$store.dispatch('increaseAmount', key)"
                               class="amount-control pl-3">+</a>
                            <span class="ml-3">шт.</span>
                        </div>
                    </div>
                </li>
                <li class="list-group-item d-flex align-items-center" v-if="typeof item.configurationValues !== 'undefined'">
                    <cart-product-configuration-mobile
                        :configuration-values="item.configurationValues">
                    </cart-product-configuration-mobile>
                </li>
                <li class="list-group-item d-flex align-items-center">
                    <div class="label flex-grow-1">Цена:</div>
                    <div class="value">{{ new Intl.NumberFormat('ru-RU').format(item.price) }} руб/шт.</div>
                </li>
                <li class="list-group-item d-flex align-items-center">
                    <div class="label flex-grow-1">Итого:</div>
                    <div class="value">{{ new Intl.NumberFormat('ru-RU').format(item.price * item.amount) }} руб</div></li>
            </ul>
        </div>
    </div>
</template>

<script>
    import {mapGetters} from "vuex"
    import CartProductConfigurationMobile from './CartProductConfigurationMobile'

    export default {
        name: "CartProductTableMobile",
        components: { CartProductConfigurationMobile },
        computed: {
            ...mapGetters(['getCart'])
        },
    }
</script>

<style scoped>
    .label {
        min-width: 40%;
    }

    .title {
        text-align: left;
        margin: 20px 0;
        font-weight: 700;
        padding-right: 50px;
    }

    .value {
        text-align: right;
    }

    .amount-control {
        font-size: 26px;
        color: #424242;
        text-decoration: none;
    }

    .amount-control,
    .amount-control + span {
        user-select: none;
    }
</style>
